# JellyFish Coding Challenge

For this challenge I have used laravel as my framework. 

Below are the links of important files.

- [app/Utilities/StringCalculator.php](app/Utilities/StringCalculator.php)
- [tests/Unit/StringCalculatorTest.php](tests/Unit/StringCalculatorTest.php)
- [composer.json](composer.json)

Some screenshots during the development process

#### Code
![Code](developmentscreens/code.png "Code")

#### Testing
![Testing](developmentscreens/unit_tests.png "Testing")

#### Tinker
![Tinker](developmentscreens/tinker.png "Tinker")


### Directions
- Please create a public github/bitbucket repository for your solution.
- Estimated time to complete is 1.5 hours
- Please use either JavaScript / PHP / .Net code (based on the position you are applying for)

### Behaviour/Test Driven Development
- Write your tests first
- Write the simplest thing that will work
- Start with the simplest test case of an empty string and move to 1 and two numbers

1. Create a simple string Calculator with the following method signature
    1. int Add(string numbers)
    2. The method can take any amount of numbers and will return their sum (return 0 for an empty string)
        i. e.g. “” or “1” or “1,2”
2. Allow the Add method to handle “|” between numbers (as well as commas)
    1. e.g. “1|2,3”
    2. There is no need to test for invalid inputs e.g. “1,|2”
3. Allow the Add method to handle a custom delimiter
    1. To change a delimiter, the beginning of the string will contain a definition that looks like this: “//[delimiter]\n[numbers...]”
        1. e.g. “//;\n1;2”
    2. The custom delimiter is optional
    3. All existing scenarios should still be supported
4. Calling Add with a negative number will throw an exception “negatives not allowed” and report the negative that was passed
    1. If there are multiple negatives show all of them in the exception message
5. Create 1 or more screenshots of the command line output proving that your code works and include this as a jpg/png in the repository
6. Please send a link to your repository back to us for evaluation
