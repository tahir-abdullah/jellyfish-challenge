<?php

namespace Tests\Unit;

use PHPUnit\Framework\TestCase;
use App\Utilities\StringCalculator;

class StringCalculatorTest extends TestCase
{
    public function test_empty_string()
    {
        $calculator = new StringCalculator;
        $this->assertEquals(0, $calculator->add(''));
    }

    public function test_add_single_number()
    {
        $calculator = new StringCalculator;
        $this->assertEquals(100, $calculator->add('100'));
    }

    public function test_add_two_numbers()
    {
        $calculator = new StringCalculator;
        $this->assertEquals(300, $calculator->add('100,200'));
    }

    public function test_add_multiple_numbers()
    {
        $calculator = new StringCalculator;
        $this->assertEquals(600, $calculator->add('100,200,300'));
    }

    public function test_allow_pipe_delimiter()
    {
        $calculator = new StringCalculator;
        $this->assertEquals(600, $calculator->add('100|200,300'));
    }

    public function test_allow_custom_delimiter()
    {
        $calculator = new StringCalculator;
        $this->assertEquals(300, $calculator->add("//;\n100;200"));
    }

    public function test_negative_number_exception()
    {
        $calculator = new StringCalculator;
        $this->expectExceptionMessage("Following -200,-400 negative numbers not allowed.");
        $calculator->add('100,-200,-400');
    }
}
