<?php

namespace App\Utilities;

use Exception;

class StringCalculator
{
    private $allowedDelimiters = ['|'];
    private $numbers = [];

    /**
     * Calculates the sum of numbers from string.
     *
     * @param  string  $numbers
     * @return int
     * @throws Exception
     */
    public function add(string $numbers): int
    {
        if (!$numbers) {
            return 0;
        }

        $this->setDelimiters($numbers)
            ->setNumbers($numbers)
            ->validateNegativeNumbers();

        /*$this->extractNumbersForFun($numbers)
            ->validateNegativeNumbers();*/

        return array_sum($this->numbers);
    }

    /**
     * Parses the string for custom delimiter
     * and adds to delimiters array.
     *
     * @param  string  $numbers
     * @return StringCalculator
     */
    private function setDelimiters(string $numbers): StringCalculator
    {
        preg_match("/\/\/(.)\\n/", $numbers, $matches);

        if (isset($matches[1])) {
            $this->allowedDelimiters[] = $matches[1];
        }

        return $this;
    }

    /**
     * Sets the numbers array for the object from provided string.
     *
     * @param  string  $numbers
     * @return StringCalculator
     */
    private function setNumbers(string $numbers): StringCalculator
    {
        if (count($this->allowedDelimiters) == 2) {
            $string = explode("\n", $numbers);
            $string = str_replace($this->allowedDelimiters, ',', $string[1]);
        } else {
            $string = str_replace($this->allowedDelimiters, ',', $numbers);
        }

        $this->numbers = explode(',', $string);

        return $this;
    }

    /**
     * Throw exception if there is negative numbers present in string.
     *
     * @throws Exception
     */
    private function validateNegativeNumbers(): void
    {
        $negativeNumber = $this->extractNegativeNumbers();

        if (count($negativeNumber)) {
            $reportNumbers = implode(',', $negativeNumber);
            throw new Exception("Following {$reportNumbers} negative numbers not allowed.");
        }
    }

    /**
     * Extract negative numbers from the provided string.
     *
     * @return array
     */
    private function extractNegativeNumbers(): array
    {
        return array_filter($this->numbers, function ($number) {
            if ($number < 0) {
                return $number;
            }
        });
    }

    /**
     * This function is just for fun. Extracts number from a string.
     * No need to do any delimiter logic. Problem will be if there
     * is a decimal number then it will mess up. It will not cover
     * negative numbers.
     *
     * @param  string  $numbers
     */
    private function extractNumbersForFun(string $numbers): void
    {
        preg_match_all("!\d+!", $numbers, $matches);
        $this->numbers = $matches[0];
    }
}
